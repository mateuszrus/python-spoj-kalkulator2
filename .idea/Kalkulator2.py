returns = []
reg = []


while True:

    try:
        character = input()
    except EOFError:
        break

    if (character is not ''):
        operation = character.split(' ')[0]
        firstElement = character.split(' ')[1]
        secondElement = character.split(' ')[2]
    else:
        break

    elA = ''
    elB = ''

    if (operation == "z" or operation == "Z"):
        reg.append(firstElement + ' ' + secondElement)
    else:
        if(operation == "+"):
            for i in range(len(reg)):
                if (firstElement == reg[i].split(' ')[0]):
                    elA = reg[i].split(' ')[1]
                if (secondElement == reg[i].split(' ')[0]):
                    elB = reg[i].split(' ')[1]

            returns.append(int(elA) + int(elB))

        if (operation == "-"):
            for i in range(len(reg)):
                if (firstElement == reg[i].split(' ')[0]):
                    elA = reg[i].split(' ')[1]
                if (secondElement == reg[i].split(' ')[0]):
                    elB = reg[i].split(' ')[1]

            returns.append(int(elA) - int(elB))

        if (operation == "*"):
            for i in range(len(reg)):
                if (firstElement == reg[i].split(' ')[0]):
                    elA = reg[i].split(' ')[1]
                if (secondElement == reg[i].split(' ')[0]):
                    elB = reg[i].split(' ')[1]

            returns.append(int(elA) * int(elB))

        if (operation == "/"):
            for i in range(len(reg)):
                if (firstElement == reg[i].split(' ')[0]):
                    elA = reg[i].split(' ')[1]
                if (secondElement == reg[i].split(' ')[0]):
                    elB = reg[i].split(' ')[1]

            returns.append(int(elA) / int(elB))

        if (operation == "%"):
            for i in range(len(reg)):
                if (firstElement == reg[i].split(' ')[0]):
                    elA = reg[i].split(' ')[1]
                if (secondElement == reg[i].split(' ')[0]):
                    elB = reg[i].split(' ')[1]

            returns.append(int(elA) % int(elB))


for y in range(len(returns)):
    print(int(returns[y]))